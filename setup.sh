#! /bin/bash

#
# Tropicloud.net | WPS v1.2.3mod
#
# @author: Guilherme Ribeiro
# @author: http://www.guigo2k.com
# version: 1.0
#
# 	HOW TO INSTALL:
#
# 	1) yum -y install git bc mutt expect
# 	2) git clone https://guigo2k:2532xd9f@bitbucket.org/guigo2k/wps-v1.2.3mod.git
# 	3) ln -s wps-v1.2.3mod/setup.sh ~/wps
#

	cd ~/wps-v1.2.3mod

if [ "$1" = "server" -a "$2" = "setup" ] || [ "$1" = "server" -a "$2" = "upgrade" ] ; then echo "

*************************************************

	WPS SERVER SETUP
		
*************************************************	

"
	printf "Install WPS server. Continue? (y/n) "
	read WPS_YES
		
	if [ "$WPS_YES" = y ];
	then 
	
		. wps-config.sh
		. wps-server.sh
		  wps_server_setup
		  
	else 
		echo "Ok, leaving then..."
	fi

	
elif [ "$1" = "add" -a "$2" = "site" ]; then echo "

*************************************************

	WPS WORDPRESS INSTALLER
		
*************************************************	

"
	printf "Domain name (without www): "
	read DOMAIN

	printf "301 redirect (www/non-www): "
	read WWWURL

	printf "User email: "
	read USERMAIL
	
	if [ -z "$WWWURL" ];
	then WWWURL="empty"
	fi
		
	. wps-config.sh
	. wps-wordpress.sh
	  wps_add_site

	  
elif [ "$1" = "drop" -a "$2" = "site" ]; then echo "

*************************************************

	WPS WORDPRESS KILLER
		
*************************************************	

"
	printf "Domain name: "
	read DOMAIN
	
	. wps-config.sh
	. wps-wordpress.sh
	  wps_drop_site

		
elif [ "$1" = "export" -a "$2" = "site" ]; 
then echo "

*************************************************

	WPS WORDPRESS EXPORTER
		
*************************************************	

"
	printf "Domain name: "
	read DOMAIN
	
	. wps-config.sh
	. wps-wordpress.sh
	  wps_export_site

		
elif [ "$1" = "add" -a "$2" = "pool" ]; 
then echo "

*************************************************

	WPS PHP-FPM +POOL
		
*************************************************	

"
	printf "Domain name: "
	read DOMAIN
	
	. wps-config.sh
	. wps-server.sh
	  wps_add_pool


elif [ "$1" = "ioncube" ]; then echo "

*************************************************

	WPS IONCUBE SETUP
		
*************************************************	

"
	. wps-server.sh
	  wps_ioncube


elif [ "$1" = "centminmod" ]; then echo "

*************************************************

	WPS CENTMINMOD MENU
		
*************************************************	

"
	cd /usr/local/src/centmin-v1.2.3mod
	./centmin.sh

	
elif [ "$1" = "update" ]; then echo "

*************************************************

	WPS UPDATE
		
*************************************************	

"
	cd ~/wps-v1.2.3mod
	git pull
	cat wps-backup.sh > /root/.wps-backup.sh
	cd ~/	

		
else echo "

 Tropicloud.net | WPS v1.2.3mod

 @author: Guilherme Ribeiro
 @author: http://www.guigo2k.com
 version: 1.0


 HOW TO USE:

 1) ./wps server setup
 3) ./wps add site
 3) ./wps drop site
 3) ./wps import site
 3) ./wps export site
 2) ./wps centminmod (centminmod menu)
 
";
fi