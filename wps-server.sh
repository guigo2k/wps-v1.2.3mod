#
# Tropicloud.net | WPS v1.2.3mod
#

function wps_server_setup() {

if [ "$2" = "setup" ]; then echo "

*************************************************

	WPS SERVER SETUP | wps-v1.2.3mod

*************************************************

"

	# download centmin-v1.2.3mod
	cd /usr/local/src
	wget http://centminmod.com/download/centmin-v1.2.3-eva2000.03.zip
	unzip centmin-v1.2.3-eva2000.03.zip
	cd centmin-v1.2.3mod
	chmod +x centmin.sh
		
	# change timezone info
	sed -i "s|ZONEINFO=Australia/Brisbane|ZONEINFO=America/Sao_Paulo|g" centmin.sh
	
	# install centmin-v1.2.3mod
	expect << EOF
set timeout -1
spawn ./centmin.sh
expect "Enter option"
send -- "1\r"
expect "Enter current password for root"
send -- "\r"
expect "Set root password"
send -- "y\r"
expect "New password"
send -- "$DB_ROOT\r"
expect "Re-enter new password"
send -- "$DB_ROOT\r"
expect "Remove anonymous users"
send -- "y\r"
expect "Disallow root login remotely"
send -- "y\r"
expect "Remove test database and access to it"
send -- "y\r"
expect "Reload privilege tables now"
send -- "y\r"
expect "Type username your want to set and press Enter"
send -- "$ADM_USER\r"
expect "Type password your want to set and press Enter"
send -- "$ADM_PASS\r"
expect eof
EOF

else echo "

*************************************************

	WPS SERVER UPGRADE | wps-v1.2.3mod

*************************************************

";
fi
	
	cd ~/wps-v1.2.3mod
	
	sed -i "s|#character-set-server=utf8|character-set-server=utf8|g" /etc/my.cnf
	sed -i "s|exec,passthru,shell_exec,system,proc_open,popen|passthru,shell_exec,system,popen|g" /usr/local/lib/php.ini	
	sed -i "s|127.0.0.1:9000|php-fpm-sock|g" /usr/local/nginx/conf/*.conf
	sed -i "s|listen = 127.0.0.1:9000|;listen = 127.0.0.1:9000|g" /usr/local/etc/php-fpm.conf
	sed -i "s|listen.allowed_clients|;listen.allowed_clients|g" /usr/local/etc/php-fpm.conf
	sed -i "s|;listen = /tmp/php5-fpm.sock|listen = /tmp/php5-fpm.sock|g" /usr/local/etc/php-fpm.conf
	sed -i "s|;listen.owner = nobody|listen.owner = nginx|g" /usr/local/etc/php-fpm.conf
	sed -i "s|;listen.group = nobody|listen.group = nginx|g" /usr/local/etc/php-fpm.conf
	sed -i "s|;listen.mode = 0666|listen.mode = 0666|g" /usr/local/etc/php-fpm.conf	
	sed -i "s|keepalive_timeout  10|keepalive_timeout  30s|g" /usr/local/nginx/conf/nginx.conf
	sed -i "s|client_max_body_size 1024k|client_max_body_size 200m|g" /usr/local/nginx/conf/nginx.conf
	sed '19 r lib/nginx.conf' /usr/local/nginx/conf/nginx.conf > /usr/local/nginx/conf/nginx-tmp.conf

	cat lib/index.html > /usr/local/nginx/html/index.html
	cat lib/wpffpc.conf > /usr/local/nginx/conf/wpffpc.conf
	cat lib/wpsecure.conf >/usr/local/nginx/conf/wpsecure.conf
	cat lib/wpnocache.conf > /usr/local/nginx/conf/wpnocache.conf
	
	cd /usr/local/nginx/conf/
	rm -rf nginx.conf
	mv nginx-tmp.conf nginx.conf
	
	# install wp-cli addon
	cd /usr/local/src/centmin-v1.2.3mod/addons/
	chmod +x wpcli.sh
	./wpcli.sh install
	
	# install newrelic sysmond
	rpm -Uvh http://download.newrelic.com/pub/newrelic/el5/i386/newrelic-repo-5-3.noarch.rpm
	yum -y install newrelic-sysmond
	nrsysmond-config --set license_key=$RPM_KEY
	/etc/init.d/newrelic-sysmond start
	
	# install s3cmd
	cd /etc/yum.repos.d
	wget http://s3tools.org/repo/RHEL_6/s3tools.repo
	yum -y install s3cmd	
	
	expect << EOF
set timeout -1
spawn s3cmd --configure
expect "Access Key:"
send -- "AKIAJOEMGIZUNFBQXYEA\r";
expect "Secret Key:"
send -- "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\r";
expect "Encryption password"
send -- "2532xd9f\r";
expect "Path to GPG program"
send -- "\r";
expect "Use HTTPS protocol"
send -- "yes\r";
expect "Test access with supplied credentials"
send -- "y\r";
expect "Save settings?"
send -- "y\r";
expect eof
EOF

	# create backup script
	cat ~/wps-v1.2.3mod/wps-backup.sh > /root/.wps-backup.sh
	chmod +x /root/.wps-backup.sh
	
	# set local time
	cat /usr/share/zoneinfo/America/Sao_Paulo > /etc/localtime
	service ntpd force-reload
	
	# set backup cron
	echo "00 6 * * * /root/.wps-backup.sh" >> s3cron
	crontab s3cron
	rm -f s3cron
	
	# clean up!
	cd ~/
	rm -f mysqlreport 
	rm -f mysqltuner.pl 
	
	# and... go!
	pscontrol on
	mysqlrestart
	nprestart
	
	echo "

*************************************************

	WPS Setup Completed

*************************************************

	HOST: $USERNAME
	IPv4: $IP	
	
	--- Tropicloud
	www.tropicloud.net

*************************************************

";

	shutdown -r now
	
}

function wps_add_pool() {

	if [ ! -d ~/centminmod/fpm.d ]; then
	mkdir -p ~/centminmod/fpm.d
	cat >> /usr/local/etc/php-fpm.conf << EOF

include=/root/centminmod/fpm.d/*.conf
EOF
	fi
	
	cp /usr/local/etc/php-fpm.conf ~/centminmod/fpm.d/fpm-$DOMAIN.conf
	sed -i "1,6 d" ~/centminmod/fpm.d/fpm-$DOMAIN.conf
	sed -i "s|listen = /tmp/php5-fpm.sock|listen = /tmp/$DOMAIN.sock|g" ~/centminmod/fpm.d/fpm-$DOMAIN.conf
	sed -i "s|include|;include|g" ~/centminmod/fpm.d/fpm-$DOMAIN.conf
	sed -i "s|www|$DOMAIN|g" ~/centminmod/fpm.d/fpm-$DOMAIN.conf
	
	cp /usr/local/nginx/conf/php.conf /usr/local/nginx/conf/php-$DOMAIN.conf
	sed -i "s|/tmp/php5-fpm.sock|/tmp/$DOMAIN.sock|g" /usr/local/nginx/conf/php-$DOMAIN.conf
	sed -i "s|php.conf|php-$DOMAIN.conf|g" /usr/local/nginx/conf/conf.d/$DOMAIN.conf
	
	echo "

*************************************************

	PHP-FPM Pool Created

*************************************************

	DOMAIN: $DOMAIN	
	LISTEN: /tmp/php5-fpm-$DOMAIN.sock	
	
	--- Tropicloud
	www.tropicloud.net

*************************************************

";

	nprestart

}

function wps_ioncube() {
	
	cd /usr/local/src
	
	wget http://downloads3.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.zip
	unzip ioncube_loaders_lin_x86-64.zip && rm -rf ioncube_loaders_lin_x86.zip
	mv ioncube/ioncube_loader_lin_5.3.so /usr/local/lib/php/extensions/no-debug-non-zts-20090626
	rm -rf ioncube
	
	cat >> /usr/local/lib/php-new.ini << EOF
zend_extension = /usr/local/lib/php/extensions/no-debug-non-zts-20090626/ioncube_loader_lin_5.3.so

EOF
	cat /usr/local/lib/php.ini >> /usr/local/lib/php-new.ini && rm -f /usr/local/lib/php.ini
	mv /usr/local/lib/php-new.ini /usr/local/lib/php.ini
	
	echo "

*************************************************

	IonCube Setup Completed

*************************************************

	ZEND: ioncube_loader_lin_5.3.so	
	PATH: /usr/local/lib/php/extensions/no-debug-non-zts-20090626
	
	--- Tropicloud
	www.tropicloud.net

*************************************************

";

	nprestart

}