#
# Tropicloud.net | WPS v1.2.3mod
#

function wps_add_site() {

	if id -u $USERNAME >/dev/null 2>&1 ;
	then echo "
*************************************************	

	User already exits, moving on...
"
	else echo "
*************************************************	
	
	Creating new WPS user...	
	
*************************************************
"
		# create chroot user	
		useradd -g nginx -d /home/$USERNAME -s /bin/false $USERNAME
		echo "$PASSWORD" | passwd "$USERNAME" --stdin
		
		mkdir /home/$USERNAME/www
		chown root:root /home/$USERNAME
		chmod 755 /home/$USERNAME
		chown nginx:nginx /home/$USERNAME/www
		chmod 775 /home/$USERNAME/www
		
		# create root shortcuts
		ln -s /home/$USERNAME/www ~/
		ln -s /usr ~/
		
		# edit ssh config
		sed -i "s/#PermitRootLogin yes/PermitRootLogin without-password/" /etc/ssh/sshd_config
		sed -i "s/Subsystem/#Subsystem/" /etc/ssh/sshd_config
		cat >> /etc/ssh/sshd_config << EOF
		
Subsystem sftp internal-sftp
AllowUsers root $USERNAME
Match User $USERNAME
    ChrootDirectory %h
    ForceCommand internal-sftp
    X11Forwarding no
    AllowTcpForwarding no
EOF
		
		# reload ssh service
		service sshd restart	
		
		# create mysql user
		mysql -u root -p"$DB_ROOT" -e "CREATE USER $USERNAME@localhost";
		mysql -u root -p"$DB_ROOT" -e "SET PASSWORD FOR $USERNAME@localhost= PASSWORD('$DB_PASS')";
		mysql -u root -p"$DB_ROOT" -e "FLUSH PRIVILEGES";
		
		# set mutt's from address
		cat >> ~/.muttrc << EOF
set from="Tropicloud <admin@tropicloud.net>"
set edit_headers=yes
EOF
		
		# send welcome email
		cat >> mail.txt << EOF
O seu WordPress Cloud Server foi configurado com sucesso.
Você pode acessar servidor via SFTP com as seguintes informações:

	Server: $IP
	Username: $USERNAME
	Password: $PASSWORD

Aproveite sua estadia no melhor hotel para sites WordPress.

-- Tropicloud
www.tropicloud.net

EOF
		
		mutt -b $ADM_MAIL -s "Novo WordPress Server" $USERMAIL < mail.txt && rm mail.txt
		
		WPSUSER=${USERNAME}
		
	fi
	echo "
*************************************************

	Installing WordPress...

*************************************************
		
";

	WP_PATH=${WW_ROOT}'/public'${SUBDIR}'/wp'	

	# create MySQL database
	mysql -u root -p"$DB_ROOT" -e "CREATE DATABASE $DB_NAME";
	mysql -u root -p"$DB_ROOT" -e "GRANT ALL PRIVILEGES ON $DB_NAME.* TO $USERNAME@localhost";
	mysql -u root -p"$DB_ROOT" -e "FLUSH PRIVILEGES";	
	
	# create vHOST folders
	mkdir -p $WW_ROOT	
	mkdir -p $WW_ROOT/backup
	mkdir -p $WW_ROOT/log
	mkdir -p $WW_ROOT/private
	mkdir -p $WW_ROOT/public
	mkdir -p $WW_ROOT/ssl
	mkdir -p $WP_PATH
	
	# create SSL cert.
	cd $WW_ROOT/ssl
	cat ~/wps-v1.2.3mod/lib/openssl.conf > openssl.conf
	sed -i "s|example.com|$DOMAIN|g" openssl.conf	
	openssl req -nodes -sha256 -newkey rsa:2048 -keyout $DOMAIN.key -out $DOMAIN.csr -config openssl.conf -batch
	cp $DOMAIN.key $DOMAIN.key.org	
	openssl rsa -in $DOMAIN.key.org -out $DOMAIN.key
	openssl x509 -req -days 365 -in $DOMAIN.csr -signkey $DOMAIN.key -out $DOMAIN.crt	
	rm openssl.conf && rm $DOMAIN.key.org
	
	# download wordpress
	cd $WP_PATH	
	wp core download --locale=pt_BR
	wp core config --dbname=$DB_NAME --dbuser=$USERNAME --dbpass=$DB_PASS
	
	# edit wp-config
	cd ~/wps-v1.2.3mod	
	sed '65 r lib/wpconfig.conf' $WP_PATH/wp-config.php > $WP_PATH/wp-config-tmp.php
	sed -i "s|DOMAIN|$DOMAIN|g" $WP_PATH/wp-config-tmp.php
	sed -i "s|/SUBDIR|$SUBDIR|g" $WP_PATH/wp-config-tmp.php
	sed -i "s|'wp_'|'${DB_PRFX}_'|g" $WP_PATH/wp-config-tmp.php	
	cd $WP_PATH
	rm -f wp-config.php
	rm -f wp-config-sample.php
	mv wp-config-tmp.php wp-config.php
	if [ $WWWURL = www ];
	then sed -i "s|http://|http://www.|g" wp-config.php 
	fi
	
	# copy wp index
	cp index.php ../index.php		
	sed -i "s|/wp-blog-header.php|/wp/wp-blog-header.php|g" ../index.php

	# install wordpress
	wp core install --url='http://'$DOMAIN'/wp' --title=$DOMAIN --admin_name=$ADM_USER --admin_email=$ADM_MAIL --admin_password=$ADM_PASS
	wp user create $USERNAME $USERMAIL --user_pass=$PASSWORD --role=administrator
	wp plugin install nginx-helper --activate
	wp plugin install cloudflare --activate
	wp plugin install wp-ffpc --activate
	wp plugin install worker
	wp plugin delete hello
	
	# fix permissions
	chown nginx:nginx $WW_ROOT/public
	chown -R nginx:nginx $WW_ROOT/public		
		
	# cerate nginx vhost
	if [ $WWWURL = www ]; then		
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
server {
	server_name DOMAIN;
	return 301 $scheme://www.DOMAIN$request_uri;
}

server {

	listen 80;
	server_name www.DOMAIN;
EOF
	elif [ $WWWURL = non-www ]; then
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
server {
	server_name www.DOMAIN;
	return 301 $scheme://DOMAIN$request_uri;
}

server {

	listen 80;
	server_name DOMAIN;
EOF
	else 
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
server {

	listen 80;
	server_name DOMAIN;
EOF
	fi
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
	
	root 			/home/USERNAME/www/DOMAIN/public;	
	access_log 		/home/USERNAME/www/DOMAIN/log/access.log combined buffer=32k;
	error_log 		/home/USERNAME/www/DOMAIN/log/error.log;
	
	include /usr/local/nginx/conf/pagespeed.conf;
	include /usr/local/nginx/conf/pagespeedhandler.conf;
	include /usr/local/nginx/conf/pagespeedstatslog.conf;	
	include /usr/local/nginx/conf/wpsecure.conf;
	include /usr/local/nginx/conf/wpnocache.conf;
	
	location /wp { return 301 https://$host$request_uri; }
	
	location / {
		
		# block common exploits, sql injections etc
		include /usr/local/nginx/conf/block.conf;
		
		# Disable directory listings when index file not found
		autoindex off;

		try_files $uri $uri/ @memcached;
	}

	include /usr/local/nginx/conf/wpffpc.conf;
	include /usr/local/nginx/conf/php.conf;
	include /usr/local/nginx/conf/staticfiles.conf;
	include /usr/local/nginx/conf/drop.conf;
	include /usr/local/nginx/conf/errorpage.conf;

}

EOF
	if [ $WWWURL = www ]; then		
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
server {

	listen 443 ssl spdy;
	server_name www.DOMAIN;
EOF
	else 		
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
server {

	listen 443 ssl spdy;
	server_name DOMAIN;	
EOF
	fi
	cat >> /usr/local/nginx/conf/conf.d/$DOMAIN.conf << 'EOF'
	
	root 					/home/USERNAME/www/DOMAIN/public;
	access_log 				/home/USERNAME/www/DOMAIN/log/access.log combined buffer=32k;
	error_log 				/home/USERNAME/www/DOMAIN/log/error.log;

	ssl_certificate			/home/USERNAME/www/DOMAIN/ssl/DOMAIN.crt;
	ssl_certificate_key		/home/USERNAME/www/DOMAIN/ssl/DOMAIN.key;

	ssl_protocols 			SSLv3 TLSv1 TLSv1.1 TLSv1.2;
	ssl_session_cache		shared:SSL:10m;
	ssl_session_timeout  	10m;
	
	add_header Alternate-Protocol 443:npn-spdy/2;
	
	include /usr/local/nginx/conf/pagespeed.conf;
	include /usr/local/nginx/conf/pagespeedhandler.conf;
	include /usr/local/nginx/conf/pagespeedstatslog.conf;	
	include /usr/local/nginx/conf/wpsecure.conf;
	include /usr/local/nginx/conf/wpnocache.conf;
	
	location / { return 301 http://$host$request_uri; }
	
	location /wp {
		
		# block common exploits, sql injections etc
		include /usr/local/nginx/conf/block.conf;
		
		# Disable directory listings when index file not found
		autoindex off;

		try_files $uri $uri/ /index.php;
	}
	
	include /usr/local/nginx/conf/wpffpc.conf;
	include /usr/local/nginx/conf/phpssl.conf;
	include /usr/local/nginx/conf/staticfiles.conf;
	include /usr/local/nginx/conf/drop.conf;
	include /usr/local/nginx/conf/errorpage.conf;

}
EOF
	sed -i "s|DOMAIN|$DOMAIN|g" /usr/local/nginx/conf/conf.d/$DOMAIN.conf
	sed -i "s|USERNAME|$USERNAME|g" /usr/local/nginx/conf/conf.d/$DOMAIN.conf
		
	# send welcome email
	cat >> mail.txt << EOF
O seu novo site WordPress foi configurado com sucesso em:

	http://$DOMAIN

Você pode fazer login na conta de administrador com as seguintes informações:
	
	http://$DOMAIN/wp/wp-admin/
	
	Username: $USERNAME
	Password: $PASSWORD

Esperamos que você aproveite seu novo site em alta velocidade ;)

-- Tropicloud
www.tropicloud.net

EOF

	mutt -b $ADM_MAIL -s "Novo site WordPress" $USERMAIL < mail.txt && rm -f mail.txt
	
	# and... go!
	nprestart
	
	if [ -z $WPSUSER ]; 
	then echo "

*************************************************

	WP INSTALL COMPLETED

*************************************************

	Site: ${DOMAIN}${SUBDIR}
	
	User: $USERNAME
	Pass: $PASSWORD
	
	--- Tropicloud
	www.tropicloud.net

*************************************************

";
	else echo "

*************************************************

	WPS SETUP COMPLETED
    
*************************************************

	WPS_
	User: $USERNAME
	Pass: $PASSWORD
         
	WP__
	Site: ${DOMAIN}${SUBDIR}
	
	User: $USERNAME
	Pass: $PASSWORD
	
	--- Tropicloud
	www.tropicloud.net

*************************************************
";
	fi
}

function wps_drop_site() {

	echo "

*************************************************	
	
	Removing $DOMAIN	
	
*************************************************		

";
			
	chown -R nginx:nginx $WW_ROOT
	echo "Fixing Permissions...... DONE!";

	rm -rf $WW_ROOT
	echo "Deleting Website Files...... DONE!";
	
	rm -rf /usr/local/nginx/conf/conf.d/$DOMAIN.conf
	echo "Deleting Virtual Host...... DONE!";
	
	mysql -u root -p"$DB_ROOT" -e "DROP DATABASE $DB_NAME";
	echo "Deleting MySQL Database...... DONE!";
	
	nprestart

}

function wps_export_site() {

	echo "

*************************************************	
	
	Exporting $DOMAIN	
	
*************************************************		

"
	  NOW=$(date +%Y-%m-%d-%H:%M)
	TOKEN=$(openssl rand -hex 8)
	
	cd ${WW_ROOT}/backup
	
	# remove previous backups
	echo "$NOW - Deleting previous backp files..." >> ${WW_ROOT}/log/backup.log
	rm -rf *
	
	# create tmp folder
	echo "$NOW - Creating tmp folder..." >> ${WW_ROOT}/log/backup.log
	mkdir -p tmp/mwp_db
	
	# dump mysql database
	echo "$NOW - Dumping MySQL database..." >> ${WW_ROOT}/log/backup.log
	mysqldump --add-drop-table -u root -p$DB_ROOT $DB_NAME > tmp/mwp_db/$DB_NAME.sql
	
	# copy website files
	echo "$NOW - Copying website files..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	if [ "$DOMAIN" = "accounts.tropicloud.net" ]; then
		cp -r ${WW_ROOT}/whmcs ${WW_ROOT}/backup/tmp/whmcs		
	else
		cp -r ${WW_ROOT}/public/wp/* ${WW_ROOT}/backup/tmp
	fi
	
	cd tmp
	
	# create zip archive
	echo "$NOW - Creating ZIP archive..." >> ${WW_ROOT}/log/backup.log
	zip -r ../${DOMAIN}_${NOW}_${TOKEN}.zip *
	
	cd ../
	
	# upload to Amazon S3
	echo "$NOW - Uploading to Amazon S3..." >> ${WW_ROOT}/log/backup.log
	s3cmd put --acl-public ${DOMAIN}_${NOW}_${TOKEN}.zip s3://wpsexport/${DOMAIN}/${DOMAIN}_${NOW}_${TOKEN}.zip

	# remove tmp folder
	echo "$NOW - Backup Completed." >> ${WW_ROOT}/log/backup.log
	rm -rf tmp

	
	echo "

*************************************************

	WP EXPORT COMPLETED

*************************************************

	Site: ${DOMAIN}
		
	--- Tropicloud
	www.tropicloud.net

*************************************************

";
}