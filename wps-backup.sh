#!/bin/bash

#
# Tropicloud.net | WPS v1.2.3mod
#

# ---------------------------------------- #
	
	TODAY=$(date +%Y-%m-%d)
	
	USERNAME=$(uname -n)
	WWW_PATH="/home/$USERNAME/www"
	WEBSITES=$(ls $WWW_PATH)

# ---------------------------------------- #

# loop through the directories:
for DOMAIN in $WEBSITES
do

	DB_ROOT="jb32edge"
	DB_NAME=${DOMAIN//./_}
	  TOKEN=$(openssl rand -hex 8)
	    NOW=$(date +%Y-%m-%d-%H:%M)
	
	cd ${WWW_PATH}/${DOMAIN}/backup
	
	# remove previous backups
	echo "$NOW - Deleting previous backp files..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	rm -rf *
	
	# create tmp folder
	echo "$NOW - Creating tmp folder..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	mkdir -p tmp/mwp_db
	
	# dump mysql database
	echo "$NOW - Dumping MySQL database..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	mysqldump --add-drop-table -u root -p$DB_ROOT $DB_NAME > tmp/mwp_db/$DB_NAME.sql
	
	# copy website files
	echo "$NOW - Copying website files..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	if [ "$DOMAIN" = "accounts.tropicloud.net" ]; then
		cp -r ${WWW_PATH}/${DOMAIN}/whmcs ${WWW_PATH}/${DOMAIN}/backup/tmp/whmcs		
	else
		cp -r ${WWW_PATH}/${DOMAIN}/public/wp/* ${WWW_PATH}/${DOMAIN}/backup/tmp
	fi
	
	cd tmp
		
	# create zip archive
	echo "$NOW - Creating ZIP archive..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	zip -r ../${DOMAIN}_${TODAY}_${TOKEN}.zip *
	
	cd ../
	
	# upload to Amazon S3
	echo "$NOW - Uploading to Amazon S3..." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	s3cmd put ${DOMAIN}_${TODAY}_${TOKEN}.zip s3://wpsbackup/${DOMAIN}/${DOMAIN}_${TODAY}_${TOKEN}.zip	

	# remove tmp folder
	echo "$NOW - Backup Completed." > ${WWW_PATH}/${DOMAIN}/log/backup.log
	rm -rf tmp
	
done